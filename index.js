var AWS = require('aws-sdk');
AWS.config.update({region: 'ap-southeast-2'});

var params = {
  Destination: { /* required */
    CcAddresses: [
      /* more items */
    ],
    ToAddresses: [
      'me@dotronglong.com',
      /* more items */
    ]
  },
  Message: { /* required */
    Body: { /* required */
      Html: {
       Charset: "UTF-8",
       Data: "This is a <strong>html</strong> message"
      },
      Text: {
       Charset: "UTF-8",
       Data: "This is a text message"
      }
     },
     Subject: {
      Charset: 'UTF-8',
      Data: 'Test email'
     }
    },
  Source: 'trong-long_do@astro.com.my', /* required */
  ReplyToAddresses: [
    /* more items */
  ],
};

var sendPromise = new AWS.SES({apiVersion: '2010-12-01'}).sendEmail(params).promise();

// Handle promise's fulfilled/rejected states
sendPromise.then(
  function(data) {
    console.log(data.MessageId);
  }).catch(
    function(err) {
    console.error(err, err.stack);
  });